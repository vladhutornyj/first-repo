<?php
namespace App\workers;

use App\interfaces\CodingInterface;
use App\interfaces\CommunicatingInterface;
use App\interfaces\DrawingInterface;
use App\interfaces\SettingInterface;
use App\interfaces\TestingInterface;
use Illuminate\Database\Eloquent\Model;

abstract class Worker extends Model
{
    const POSITION_TO_MODEL = [
        'manager' => Manager::class,
        'programmer' => Programmer::class,
        'designer' => Designer::class,
        'tester' => Tester::class,
    ];

    public function getSkills()
    {
        $skills = [];
        if($this instanceof SettingInterface){
            $skills[] = self::SETTING;
        }
        if($this instanceof CodingInterface){
            $skills[] = self::CODING;
        }
        if($this instanceof CommunicatingInterface){
            $skills[] = self::COMMUNICATING;
        }
        if($this instanceof DrawingInterface){
            $skills[] = self::DRAWING;
        }
        if($this instanceof TestingInterface){
            $skills[] = self::TESTING;
        }
        return $skills;
    }

    // skills
    const CODING = 'coding';
    const DRAWING = 'drawing';
    const SETTING = 'setting';
    const TESTING = 'testing';
    const COMMUNICATING = 'communicating';

}