<?php
namespace App\workers;

use App\interfaces\CommunicatingInterface;
use App\interfaces\SettingInterface;
use App\interfaces\TestingInterface;

class Tester extends Worker
implements TestingInterface, CommunicatingInterface, SettingInterface
{

}