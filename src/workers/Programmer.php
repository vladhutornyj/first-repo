<?php
namespace App\workers;

use App\interfaces\CodingInterface;
use App\interfaces\CommunicatingInterface;
use App\interfaces\TestingInterface;

class Programmer extends Worker
implements CodingInterface, TestingInterface, CommunicatingInterface
{

}