<?php
namespace App\commands;

use App\workers\Worker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

class WorkerSkillsCommand extends Command
{
    protected function configure()
    {
        $this->setName('company:employee')
            ->setDescription('Get to know about employee skills')
            ->setHelp('Get to know about employee skills')
            ->addArgument('position', InputArgument::OPTIONAL, 'manager|designer|programmer|tester');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $position = $input->getArgument('position');

        //if InputArgument::OPTIONAL
        if (empty($position)) {
            $output->writeln('<error>Empty position</error>');
            exit();
        } else {
            if (isset(Worker::POSITION_TO_MODEL[$position])) {
                $className = Worker::POSITION_TO_MODEL[$position];
                $class = new $className();
                $output->writeln($class->getSkills());
            } else {
                $output->writeln('<error>No such position</error>');
            }
        }
        return 0;
    }
}