<?php

namespace App\commands;

use App\workers\Worker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

class WorkerCanCommand extends Command
{
    protected function configure()
    {
        $this->setName('employee:can')
            ->setDescription('Get to know what employee can')
            ->setHelp('Get to know what employee can')
            ->addArgument('position', InputArgument::REQUIRED, 'manager|designer|programmer|tester')
            ->addArgument('skill', InputArgument::REQUIRED, 'coding|drawing|setting|testing|communicating');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $position = $input->getArgument('position');
        $skill = $input->getArgument('skill');

        if (isset(Worker::POSITION_TO_MODEL[$position])) {
            $className = Worker::POSITION_TO_MODEL[$position];
            $class = new $className();
            $skills = $class->getSkills() ?? [];
            if(in_array($skill, $skills)) {
                $output->writeln('true');
            } else {
                $output->writeln('false');
            }
        } else {
            $output->writeln('<error>No such position</error>');
        }

        return 0;
    }
}